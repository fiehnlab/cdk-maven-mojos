package com.blogspot.codingandmore;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.deployer.ArtifactDeployer;
import org.apache.maven.artifact.deployer.ArtifactDeploymentException;
import org.apache.maven.artifact.metadata.ArtifactMetadata;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryFactory;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.artifact.ProjectArtifactMetadata;

import java.io.File;


/**
 * deploys the cdk jar to a specified repository
 * User: wohlgemuth
 * Date: Feb 5, 2010
 * Time: 2:14:29 PM
 *
 * @goal deploy
 * @phase process-resources
 * @requiresProject false
 */
public class CDKDeployMojo extends AbstractFileMojo {
    /**
     * @component
     */
    private org.apache.maven.artifact.factory.ArtifactFactory artifactFactory;

    /**
     * @component
     */
    private org.apache.maven.artifact.resolver.ArtifactResolver resolver;

    /**
     * @parameter default-value="${localRepository}"
     */
    private org.apache.maven.artifact.repository.ArtifactRepository localRepository;

    /**
     * @component
     */
    private ArtifactDeployer deployer;

    /**
     * @parameter expression="${repositoryId}"
     */
    private String repositoryId = "googlecode";


    /**
     * @parameter expression="${repositoryUrl}"
     */
    private String repositoryUrl = "svn:https://binbase-maven.googlecode.com/svn/trunk/repo";

    /**
     * Component used to create a repository
     *
     * @component
     */
    private ArtifactRepositoryFactory repositoryFactory;

    /**
     * The pom file of the artifact to deploy.
     *
     * @parameter expression = "${pomFile}
     */
    private File pomFile;

    /**
     * does something with the found file
     *
     * @param currentJar
     * @param name
     */
    @Override
    protected void doFileAction(File currentJar, String name) throws MojoExecutionException, MojoFailureException {
        getLog().info("found: " + name);

        Artifact artifact = artifactFactory.createArtifact(this.getGroup(), name, getVersion(), null, "jar");

        ArtifactMetadata metadata = new ProjectArtifactMetadata(artifact, pomFile);
        //artifact.addMetadata(metadata);

        getLog().debug("deploying: " + artifact.getArtifactId() + " - " + artifact.getGroupId() + " - " + artifact.getVersion());

        try {
            ArtifactRepository deploymentRepository = repositoryFactory.createDeploymentArtifactRepository(repositoryId, repositoryUrl, new DefaultRepositoryLayout(), true);

            this.deployer.deploy(currentJar, artifact, deploymentRepository, localRepository);
        } catch (Exception e) {
            getLog().error(e);
            throw new MojoFailureException(e.getMessage());
        }


    }
}
