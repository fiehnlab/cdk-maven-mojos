package com.blogspot.codingandmore;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.io.File;
import java.io.FilenameFilter;

/**
 * User: wohlgemuth
 * Date: Feb 5, 2010
 * Time: 2:15:11 PM
 */
public abstract class AbstractFileMojo extends AbstractMojo {

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @parameter expression="${group}"
     * registered group for cdk
     */
    private String group = "org.openscience.cdk";

    /**
     * the version of the cdk to install
     *
     * @parameter expression="${version}"
     * @required
     */
    private String version = null;

    public String getCdkDirectory() {
        return cdkDirectory;
    }

    public void setCdkDirectory(String cdkDirectory) {
        this.cdkDirectory = cdkDirectory;
    }

    /**
     * contains the base directory of the cdk libraries
     *
     * @parameter expression="${cdkDirectory}"
     * @required
     */

    private String cdkDirectory = null;

    /**
     * does the actual execution
     *
     * @throws org.apache.maven.plugin.MojoExecutionException
     * @throws org.apache.maven.plugin.MojoFailureException
     */
    public void execute() throws MojoExecutionException, MojoFailureException {
        File dir = new File(cdkDirectory);

        if (dir.exists()) {
            File dist = new File(dir, "dist");

            //check that the dist directory exist
            if (dist.exists()) {
                File jar = new File(dist, "jar");

                //check if the jar direcotry exist
                if (jar.exists()) {

                    //get all jar files
                    for (File currentJar : jar.listFiles(new FilenameFilter() {
                        /**
                         * Tests if a specified file should be included in a file list.
                         *
                         * @param dir  the directory in which the file was found.
                         * @param name the name of the file.
                         * @return <code>true</code> if and only if the name should be
                         *         included in the file list; <code>false</code> otherwise.
                         */
                        public boolean accept(File dir, String name) {
                            return name.endsWith(".jar");
                        }
                    })) {

                        String name = currentJar.getName().replaceAll(".jar","");

                        doFileAction(currentJar, name);

                    }
                } else {
                    throw new MojoFailureException("sorry the directory does not exist, " + jar.getAbsolutePath());
                }
            } else {
                throw new MojoFailureException("sorry the directory does not exist, " + dist.getAbsolutePath());
            }
        } else {
            throw new MojoFailureException("sorry the directory does not exist, " + dir.getAbsolutePath());
        }
    }

    /**
     * does something with the found file
     * 
     * @param currentJar
     * @param name
     */
    protected abstract void doFileAction(File currentJar, String name) throws MojoExecutionException, MojoFailureException;
}
