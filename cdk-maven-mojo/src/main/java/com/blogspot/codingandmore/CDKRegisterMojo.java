package com.blogspot.codingandmore;

import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.installer.ArtifactInstallationException;
import org.apache.maven.artifact.installer.ArtifactInstaller;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.io.File;
import java.io.FilenameFilter;

/**
 * register the cdk library in the local maven repository
 * <p/>
 * User: wohlgemuth
 * Date: Feb 3, 2010
 * Time: 12:13:24 PM
 * @goal register
 *
 * @phase process-resources
 * @requiresProject false
 */
public class CDKRegisterMojo extends AbstractFileMojo {


    /**
     * @parameter default-value="${localRepository}"
     */
    private ArtifactRepository localRepository;

    /**
     * @component
     */
    private ArtifactInstaller installer;


    /**
     * installs the file in the local repository
     * @param currentJar
     * @param name
     * @throws MojoExecutionException
     * @throws MojoFailureException
     */
    protected void doFileAction(File currentJar, String name) throws MojoExecutionException, MojoFailureException {
        getLog().info("found: " + name);

        DefaultArtifactHandler handler = new DefaultArtifactHandler("jar");
        DefaultArtifact artifact = new DefaultArtifact(this.getGroup(), name, VersionRange
                .createFromVersion(getVersion()), null, "jar", null, handler);
        artifact.setFile(currentJar);

        getLog().debug("installing: " + artifact.getArtifactId() + " - " + artifact.getGroupId() + " - " + artifact.getVersion());

        if (localRepository == null) {
            throw new MojoExecutionException("no local repository was specified!!!");
        }
        try {
            this.installer.install(currentJar, artifact, localRepository);
        } catch (ArtifactInstallationException e) {
            throw new MojoFailureException(e.getMessage());
        }
    }
}
